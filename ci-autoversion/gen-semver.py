#!/usr/bin/env python3
import os
import re
import sys
import subprocess


def git(*args):
    return subprocess.run(["git"] + list(args), check=True, capture_output=True, text=True).stdout

def increment_version(version, release_type):
    # Split the version string into its components
    version_tag = version.split('-')
    version_parts = version_tag[0].split('.')
    
    # Increment the version number based on the release type
    if release_type == 'patch':
        version_parts[2] = str(int(version_parts[2]) + 1)
    elif release_type == 'minor':
        version_parts[1] = str(int(version_parts[1]) + 1)
        version_parts[2] = '0'
    elif release_type == 'major':
        version_parts[0] = str(int(version_parts[0]) + 1)
        version_parts[1] = '0'
        version_parts[2] = '0'
    else:
        raise ValueError('Invalid release type')
    
    # Rejoin the version components and return the new version string
    return '.'.join(version_parts)

def bump(latest, message):
    # increment version if commit message contains tags
    if message.find('#major') != -1:
        return increment_version(latest, "major")
    if message.find('#minor') != -1:
        return increment_version(latest, "minor")
    else:
        return increment_version(latest, "patch")


def main():
    try:
        tag = git("rev-list", "--tags", "--max-count=1")
        latest = git("describe", "--tags", tag.strip()).strip()

    except subprocess.CalledProcessError:
        # No tags in the repository
        version = "0.0.1"
    else:
        # Skip already tagged commits
        # if '-' not in latest:
        #     print(latest)
        #     return 0
        version = bump(latest,sys.argv[1])

    print(version)

    return 0


if __name__ == "__main__":
    sys.exit(main())
